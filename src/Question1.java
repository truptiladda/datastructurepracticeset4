
/*Write code to move all zeros of an input array of integers towards the end of the
array. The order of the non-zero numbers should not be altered.*/

public class Question1 {

	static void pushZerosToEnd(int[] arr) {
		int count = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != 0) {
				arr[count] = arr[i];
				count++;
			}

		}
		while (count < arr.length) {
			arr[count++] = 0;
		}

		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");

		}
	}

	public static void main(String[] args) {
		int arr[] = { 6, 0, -3, 0, 0, 4 };
		pushZerosToEnd(arr);

	}

}
