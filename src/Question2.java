import java.util.ArrayList;
import java.util.List;

/*Given a 2D array, print it in spiral form*/
public class Question2 {

	static List<Integer> printSpiral(int[][] matrix) {

		List<Integer> result = new ArrayList<>();
		int rowBegin = 0;
		int rowEnd = matrix.length - 1;
		int ColumnBegin = 0;
		int columnEnd = matrix[0].length - 1;

		if (matrix.length == 0) {
			return result;
		}

		while (rowBegin <= rowEnd && ColumnBegin <= columnEnd) {
			for (int i = ColumnBegin; i <= columnEnd; i++) {
				result.add(matrix[rowBegin][i]);
			}
			rowBegin++;
			for (int i = rowBegin; i <= rowEnd; i++) {
				result.add(matrix[i][columnEnd]);

			}
			columnEnd--;

			if (rowBegin <= rowEnd) {
				for (int i = columnEnd; i >= ColumnBegin; i--) {
					result.add(matrix[rowEnd][i]);

				}

			}
			rowEnd--;
			if (ColumnBegin <= columnEnd) {
				for (int i = rowEnd; i >= rowBegin; i--) {
					result.add(matrix[i][ColumnBegin]);
				}
			}
			ColumnBegin++;

		}

		return result;

	}

	public static void main(String args[]) {

		int[][] matrix = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 16 }

		};

		List<Integer> res = printSpiral(matrix);
		System.out.println(res);

	}

}
